package com.ukefu.webim.service.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;

import com.ukefu.webim.web.model.QueSurveyResultQuestion;
import org.springframework.data.jpa.repository.Query;

import java.util.Date;

public abstract interface QueSurveyResultQuestionRepository extends JpaRepository<QueSurveyResultQuestion, String>{

  public abstract QueSurveyResultQuestion findByIdAndOrgi(String id, String orgi);
  
  public abstract Page<QueSurveyResultQuestion> findAll(Specification<QueSurveyResultQuestion> spec, Pageable page) ;

  public abstract  Long countByQuestionid(String questionid);

  @Query(value = "SELECT count(s.id) FROM uk_que_result_question s WHERE 1=1 AND orgi = ?4 AND " +
          "IF (?1!='',s.createtime >= ?1,1=1) AND IF (?2!='',s.createtime <= ?2,1=1)  AND IF (?3!='',s.questionid = ?3,1=1) "
          ,nativeQuery = true)
  public abstract Long countByQuestionid(Date dateStart, Date dateEnd,String questionid ,String orgi) ;
}
