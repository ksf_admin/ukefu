package com.ukefu.webim.service.repository;

import com.ukefu.webim.web.model.Extention;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ExtentionRepository extends JpaRepository<Extention, String> {
	
	public Extention findByIdAndOrgi(String id , String orgi);

	public Extention findById(String id);

	public List<Extention> findByHostid(String hostid);

	public List<Extention> findByHostidAndOrgi(String hostid , String orgi);
	
	public List<Extention> findByExtentionAndOrgi(String extention, String orgi);
	
	public List<Extention> findByExtention(String extention);
	
	public List<Extention> findByHostidAndExtypeAndOrgi(String hostid , String extype, String orgi);
	
	public List<Extention> findByHostidAndExtentionAndOrgi(String hostid , String extention, String orgi);
	
	public List<Extention> findByExtypeAndOrgi(String type, String orgi);
	
	public List<Extention> findByExtypeAndHostid(String type, String hostid);
	
	public List<Extention> findByExtypeAndHostidAndOrgi(String type, String hostid, String orgi);
	
	public int countByExtentionAndHostidAndOrgi(String extention ,String hosti, String orgi) ;

	public int countByExtentionAndHostid(String extention ,String hosti) ;

	public List<Extention> findByHostidAndExtypeAndAssignedOrderByExtentionAsc(String hostid , String extype,boolean assigned);

	public Page<Extention> findAll(Specification<Extention> spec, Pageable paramPageable) ;

	public List<Extention> findAll(Specification<Extention> spec) ;
}
