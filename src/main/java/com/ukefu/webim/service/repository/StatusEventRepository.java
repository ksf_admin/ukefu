package com.ukefu.webim.service.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;

import com.ukefu.webim.web.model.StatusEvent;

public interface StatusEventRepository extends JpaRepository<StatusEvent, String> {

	public StatusEvent findById(String id);
	
	public List<StatusEvent> findByCoreuuid(String coreuuid);
	
	public StatusEvent findByIdOrBridgeid(String id,String bridgeid);
	
	public Page<StatusEvent> findByAniAndOrgi(String ani , String orgi, Pageable page) ;
	
	public List<StatusEvent> findByCode(String ani) ;
	
	public Page<StatusEvent> findByNameid(String nameid , Pageable page) ;
	
	public Page<StatusEvent> findByDataidAndOrgi(String dataid ,String orgi , Pageable page) ;
	
	public Page<StatusEvent> findByOrgi(String orgi , Pageable page) ;
	
	public Page<StatusEvent> findByServicestatusAndOrgi(String servicestatus ,String orgi , Pageable page) ;
	
	public Page<StatusEvent> findByMisscallAndOrgi(boolean misscall ,String orgi , Pageable page) ;
	
	public Page<StatusEvent> findByRecordAndOrgi(boolean record ,String orgi , Pageable page) ;
	
	public Page<StatusEvent> findByCalledAndOrgi(String voicemail ,String orgi , Pageable page) ;
	
	public Page<StatusEvent> findAll(Specification<StatusEvent> spec, Pageable pageable);  //分页按条件查询 

	public int countByAgentAndOrgi(String agent ,String orgi) ;
	
	public int countByOrgiAndAniOrCalled(String orgi ,String ani,String called);
	
	public int countByAniAndOrgi(String ani ,String orgi);
	
	public int countByCalledAndOrgi(String called ,String orgi);
	
	public int countByIdAndOrgi(String id ,String orgi);
	
	public Page<StatusEvent> findByRecordAndUseridAndOrgi(boolean record ,String userid,String orgi , Pageable page) ;
	
	public List<StatusEvent> findByTemplateidAndQualitystatusAndOrgi(String templateid,String qualitystatus ,String orgi) ;
	
	public List<StatusEvent> findByConferenceAndConferenceidAndOrgiAndCreaterNotAndConferenceinitiatorNot(boolean conference,String conferenceid ,String orgi , String creater , boolean includeInit) ;
	
	public StatusEvent findByIdAndOrgi(String id, String orgi);
	
	public List<StatusEvent> findAll(Specification<StatusEvent> spec);
	
	public List<StatusEvent> findByQualitydisuserAndQualitystatusAndOrgi(String qualitydisuser,String qualitystatus ,String orgi) ;
	
	public List<StatusEvent> findByDataidAndOrgi(String dataid ,String orgi) ;

	public Page<StatusEvent> findByOssstatusAndRecordAndOrgiAndRecordfileNotNull(String ossstatus ,boolean record,String orgi , Pageable page) ;
	
	public List<StatusEvent> findByDataidAndOrgiAndQualityresultIsNull(String dataid ,String orgi) ;
}
