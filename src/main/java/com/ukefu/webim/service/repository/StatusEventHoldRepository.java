package com.ukefu.webim.service.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ukefu.webim.web.model.StatusEventHold;

public interface StatusEventHoldRepository extends JpaRepository<StatusEventHold, String> {

}
