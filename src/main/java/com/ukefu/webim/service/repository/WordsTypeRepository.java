package com.ukefu.webim.service.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ukefu.webim.web.model.WordsType;

public abstract interface WordsTypeRepository extends
		JpaRepository<WordsType, String> {
	
	public abstract WordsType findByIdAndOrgi(String id, String orgi);

	public abstract int countByNameAndOrgi(String name, String orgi);
	
	public abstract List<WordsType> findByOrgi(String orgi) ;
	
	public abstract List<WordsType> findByCodeAndOrgi(String code ,String orgi) ;

}
