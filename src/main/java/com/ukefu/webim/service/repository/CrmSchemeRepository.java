package com.ukefu.webim.service.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ukefu.webim.web.model.CrmScheme;

public abstract interface CrmSchemeRepository extends JpaRepository<CrmScheme, String> {
	
	public abstract List<CrmScheme> findByOrgi(String orgi);
	
	public abstract CrmScheme findByIdAndOrgi(String id,String orgi);
	
	public abstract CrmScheme findByCodeAndOrgi(String code,String orgi);
	
	public int countByNameAndOrgi(String name,String orgi);
	
	public int countByCodeAndOrgi(String code,String orgi);

}
