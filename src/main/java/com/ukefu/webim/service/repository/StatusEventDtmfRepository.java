package com.ukefu.webim.service.repository;

import com.ukefu.webim.web.model.StatusEventDtmf;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StatusEventDtmfRepository extends JpaRepository<StatusEventDtmf, String> {

}
