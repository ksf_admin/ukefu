package com.ukefu.webim.service.repository;

import java.util.List;

import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;

import com.ukefu.webim.web.model.ContactsItem;

public abstract interface ContactsItemRepository extends JpaRepository<ContactsItem, String>
{
  public abstract ContactsItem findByIdAndOrgi(String id, String orgi);
  
  public abstract List<ContactsItem> findByOrgi(String orgi);
  
  public abstract List<ContactsItem> findByItemidAndOrgi(String itemid,String orgi);
  
  public abstract List<ContactsItem> findByParentidAndOrgi(String parentid,String orgi);
  
  public abstract List<ContactsItem> findByTypeAndOrgi(String type,String orgi);
  
  public abstract List<ContactsItem> findAll(Specification<ContactsItem> spec,Sort sort) ;
	
  public abstract Page<ContactsItem> findAll(Specification<ContactsItem> spec , Pageable pageRequest) ;
  
  public abstract List<ContactsItem> findByCreaterAndOrgi(String creater,String orgi);
  
  public abstract List<ContactsItem> findByOrgiOrderByCreatetimeDesc(String orgi);
  
  public abstract List<ContactsItem> findByOrgiAndParentidIsNotNullOrderByCreatetimeDesc(String orgi);
  
  public abstract List<ContactsItem> findByAutorecycle(boolean autorecycle);
  
}
