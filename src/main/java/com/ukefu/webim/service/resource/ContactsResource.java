package com.ukefu.webim.service.resource;

import static org.elasticsearch.index.query.QueryBuilders.termQuery;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.QueryStringQueryBuilder;
import org.elasticsearch.index.query.QueryStringQueryBuilder.Operator;
import org.elasticsearch.index.query.RangeQueryBuilder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ukefu.core.UKDataContext;
import com.ukefu.util.UKTools;
import com.ukefu.webim.service.es.ContactsRepository;
import com.ukefu.webim.web.model.Contacts;
import com.ukefu.webim.web.model.DisModel;
import com.ukefu.webim.web.model.DisTarget;
import com.ukefu.webim.web.model.FilterCriteria;
import com.ukefu.webim.web.model.JobDetail;

public class ContactsResource extends Resource{
	
	private ContactsRepository contactsRes ;
	
	private JobDetail jobDetail ;
	private ObjectMapper objectMapper = new ObjectMapper();  
	
	private List<DisModel> disModelList = new ArrayList<DisModel>() ;
	
	private List<Contacts> dataList = new ArrayList<Contacts>() ;
	
	private FilterCriteria filterCriteria = null ;
	
	public ContactsResource(JobDetail jobDetail) {
		this.jobDetail = jobDetail ;
		this.contactsRes = UKDataContext.getContext().getBean(ContactsRepository.class) ;
	}

	@Override
	public void begin() throws Exception {
		if(!StringUtils.isBlank(jobDetail.getJdbcurl())) {
			filterCriteria = objectMapper.readValue(jobDetail.getJdbcurl(), FilterCriteria.class) ;
		}
		if(!StringUtils.isBlank(jobDetail.getArea())) {
			DisTarget disTarget = objectMapper.readValue(jobDetail.getArea(), DisTarget.class) ;
			if(disTarget!=null) {
				if(disTarget.getUseridList()!=null && disTarget.getUseridList().size() > 0) {
					for(String userid  : disTarget.getUseridList()) {
						disModelList.add(new DisModel(userid, "user")) ;
					}
				}
				if(disTarget.getOrganidList()!=null && disTarget.getOrganidList().size() > 0) {
					for(String organ  : disTarget.getOrganidList()) {
						disModelList.add(new DisModel(organ, "organ")) ;
					}
				}
			}
		}
		if(filterCriteria!=null) {
			Page<Contacts> temp = null;
			if (filterCriteria.isToday()) {
				temp = this.contactsRes.findByQueryAndOrgi(null,this.jobDetail.getOrgi(), UKTools.getStartTime() , null , false, this.doIndex(filterCriteria.getQ(), filterCriteria.getCkind(), filterCriteria.getItemid()) ,filterCriteria.getQ() , new PageRequest(0 , 100)) ;
			}else if (filterCriteria.isWeek()) {
				temp = this.contactsRes.findByQueryAndOrgi(null,this.jobDetail.getOrgi(), UKTools.getWeekStartTime() , null , false, this.doIndex(filterCriteria.getQ(), filterCriteria.getCkind(), filterCriteria.getItemid()) ,filterCriteria.getQ() , new PageRequest(0 , 100)) ;
			}else if (filterCriteria.isNotcall()) {
				BoolQueryBuilder boolQueryBuilder = this.doIndex(filterCriteria.getQ(), filterCriteria.getCkind(), filterCriteria.getItemid());
				boolQueryBuilder.mustNot(termQuery("callstatus",UKDataContext.NameStatusTypeEnum.CALLED.toString())) ;
				temp = this.contactsRes.findByQueryAndOrgi(null,this.jobDetail.getOrgi(), null , null , false, boolQueryBuilder ,filterCriteria.getQ() , new PageRequest(0 , 100)) ;
			}else if (filterCriteria.isCalled()) {
				BoolQueryBuilder boolQueryBuilder = this.doIndex(filterCriteria.getQ(), filterCriteria.getCkind(), filterCriteria.getItemid());
				boolQueryBuilder.must(termQuery("callstatus",UKDataContext.NameStatusTypeEnum.CALLED.toString())) ;
				temp = this.contactsRes.findByQueryAndOrgi(null,this.jobDetail.getOrgi(), null , null , false, boolQueryBuilder ,filterCriteria.getQ() , new PageRequest(0 , 100)) ;
			}else if (filterCriteria.isCallsuccess()) {
				BoolQueryBuilder boolQueryBuilder = this.doIndex(filterCriteria.getQ(), filterCriteria.getCkind(), filterCriteria.getItemid());
				boolQueryBuilder.must(termQuery("callstatus",UKDataContext.NameStatusTypeEnum.CALLED.toString())) ;
		    	boolQueryBuilder.must(termQuery("workstatus", UKDataContext.NamesCalledEnum.SUCCESS.toString())) ;
				temp = this.contactsRes.findByQueryAndOrgi(null,this.jobDetail.getOrgi(), null , null , false, boolQueryBuilder ,filterCriteria.getQ() , new PageRequest(0 , 100)) ;
			}else if (filterCriteria.isCallfaild()) {
				BoolQueryBuilder boolQueryBuilder = this.doIndex(filterCriteria.getQ(), filterCriteria.getCkind(), filterCriteria.getItemid());
				boolQueryBuilder.must(termQuery("callstatus",UKDataContext.NameStatusTypeEnum.CALLED.toString())) ;
		    	boolQueryBuilder.must(termQuery("workstatus", UKDataContext.NamesCalledEnum.FAILD.toString())) ;
		    	if(!StringUtils.isBlank(filterCriteria.getHangupcase())){
	    			boolQueryBuilder.must(termQuery("hangupcase", filterCriteria.getHangupcase())) ;
		    	}
				temp = this.contactsRes.findByQueryAndOrgi(null,this.jobDetail.getOrgi(), null , null , false, boolQueryBuilder ,filterCriteria.getQ() , new PageRequest(0 , 100)) ;
			}else if (filterCriteria.isNotdis()) {
				BoolQueryBuilder boolQueryBuilder = this.doIndex(filterCriteria.getQ(), filterCriteria.getCkind(), filterCriteria.getItemid());
				boolQueryBuilder.mustNot(termQuery("status", UKDataContext.NamesDisStatusType.DISAGENT.toString())) ;
		    	boolQueryBuilder.mustNot(termQuery("status", UKDataContext.NamesDisStatusType.DISORGAN.toString())) ;
		    	boolQueryBuilder.mustNot(termQuery("status", UKDataContext.NamesDisStatusType.DISAI.toString())) ;
		    	boolQueryBuilder.mustNot(termQuery("status", UKDataContext.NamesDisStatusType.DISFORECAST.toString())) ;
				temp = this.contactsRes.findByQueryAndOrgi(null,this.jobDetail.getOrgi(), null , null , false, boolQueryBuilder ,filterCriteria.getQ() , new PageRequest(0 , 100)) ;
			}else if (!StringUtils.isBlank(filterCriteria.getDisagent())) {
				BoolQueryBuilder boolQueryBuilder = this.doIndex(filterCriteria.getQ(), filterCriteria.getCkind(), filterCriteria.getItemid());
				boolQueryBuilder.must(termQuery("status", UKDataContext.NamesDisStatusType.DISAGENT.toString())) ;
				temp = this.contactsRes.findByQueryAndOrgi(null,this.jobDetail.getOrgi(), null , null , false, boolQueryBuilder ,filterCriteria.getQ() , new PageRequest(0 , 100)) ;
			}else if (!StringUtils.isBlank(filterCriteria.getDisorgan())) {
				BoolQueryBuilder boolQueryBuilder = this.doIndex(filterCriteria.getQ(), filterCriteria.getCkind(), filterCriteria.getItemid());
				boolQueryBuilder.must(termQuery("ownerdept", filterCriteria.getDisorgan())) ;
				temp = this.contactsRes.findByQueryAndOrgi(null,this.jobDetail.getOrgi(), null , null , false, boolQueryBuilder ,filterCriteria.getQ() , new PageRequest(0 , 100)) ;
			}else if (filterCriteria.isReserved()) {
				BoolQueryBuilder boolQueryBuilder = this.doIndex(filterCriteria.getQ(), filterCriteria.getCkind(), filterCriteria.getItemid());
				boolQueryBuilder.must(termQuery("reservation", true)) ;
				temp = this.contactsRes.findByQueryAndOrgi(null,this.jobDetail.getOrgi(), null , null , false, boolQueryBuilder ,filterCriteria.getQ() , new PageRequest(0 , 100)) ;
			}else {
				temp = this.contactsRes.findByQueryAndOrgi(null,this.jobDetail.getOrgi(), null , null , false, this.doIndex(filterCriteria.getQ(), filterCriteria.getCkind(), filterCriteria.getItemid()) ,filterCriteria.getQ() , new PageRequest(0 , 100)) ;
			}
			if(temp != null && temp.getTotalElements() > 0) {
				dataList.addAll(temp.getContent()) ;
				if(temp.getTotalElements() > temp.getContent().size() && jobDetail.getReport().getAtompages().intValue() < this.jobDetail.getDisnum()) {
					jobDetail.getReport().setRound(true);
				}
			}
		}
	}

	@Override
	public void end(boolean clear) throws Exception {
		jobDetail.setNamenum(jobDetail.getReport().getAtompages().intValue());
	}

	@Override
	public JobDetail getJob() {
		return this.jobDetail;
	}

	@Override
	public void process(OutputTextFormat meta, JobDetail job) throws Exception {
		if(meta!=null && meta.getData().get("data")!=null) {
			DisModel current = (DisModel) meta.getObject() ;
			Contacts contacts = (Contacts) meta.getData().get("data") ;
			if(UKDataContext.ContactsJobDetailType.DIS.toString().equals(this.jobDetail.getActype()) && current != null) {
				contacts.setDistime(new Date());
				contacts.setDiscount(contacts.getDiscount()+1);
				if("user".equals(current.getType())) {
					contacts.setStatus(UKDataContext.NamesDisStatusType.DISAGENT.toString());
					contacts.setOwneruser(current.getDataid());
				}else if("organ".equals(current.getType())) {
					contacts.setStatus(UKDataContext.NamesDisStatusType.DISORGAN.toString());
					contacts.setOwnerdept(current.getDataid());
				}
				contactsRes.save(contacts) ;
			}else if(UKDataContext.ContactsJobDetailType.RECYCLE.toString().equals(this.jobDetail.getActype())) {
				contacts.setDistime(new Date());
				contacts.setStatus(UKDataContext.NamesDisStatusType.NOT.toString());
				contacts.setOwneruser(null);
				contacts.setOwnerdept(null);
				contactsRes.save(contacts) ;
			}
		}
	}

	@Override
	public OutputTextFormat next() throws Exception {
		DisModel current = null ;
		if(this.disModelList!=null && this.disModelList.size() > 0) {
			current = this.disModelList.remove(0) ;
		}
		OutputTextFormat outputTextFormat = null;
		if(this.dataList!=null && (this.jobDetail.getDisnum() == 0 || jobDetail.getReport().getAtompages().intValue() < this.jobDetail.getDisnum())) {
			synchronized (this) {
				if(this.dataList.size() >0 && (this.jobDetail.getDisnum() == 0 || jobDetail.getReport().getAtompages().intValue() < this.jobDetail.getDisnum())) {
					Contacts contacts = this.dataList.remove(0) ;
					outputTextFormat = new OutputTextFormat(this.jobDetail);
					outputTextFormat.setObject(current);
					outputTextFormat.getData().put("data", contacts) ;
				}
			}
		}
		if (current != null) {
			this.disModelList.add(current) ;
		}
		return outputTextFormat;
	}

	@Override
	public boolean isAvailable() {
		return true;
	}

	@Override
	public OutputTextFormat getText(OutputTextFormat object) throws Exception {
		return object;
	}

	@Override
	public void rmResource() {
		
	}

	@Override
	public void updateTask() throws Exception {
		
	}
	
	public BoolQueryBuilder doIndex(String q , String ckind, String itemid) {
    	BoolQueryBuilder boolQueryBuilder = QueryBuilders.boolQuery();
    	if(!StringUtils.isBlank(q)){
			q = q.replaceAll("(OR|AND|NOT|:|\\(|\\))", "") ;
			boolQueryBuilder.must(QueryBuilders.boolQuery().must(new QueryStringQueryBuilder(q).defaultOperator(Operator.AND))) ;
        }
    	if(!StringUtils.isBlank(ckind) && !ckind.equals("null")){
    		boolQueryBuilder.must(termQuery("ckind" , ckind)) ;
        }
    	
    	
    	if(!StringUtils.isBlank(itemid) && !itemid.equals("null")){
    		boolQueryBuilder.must(termQuery("itemid" , itemid)) ;
        }
    	
    	if(filterCriteria.getContactsids()!=null && filterCriteria.getContactsids().size() > 0) {
    		BoolQueryBuilder idsBooleanQueryBuilder = QueryBuilders.boolQuery();
    		for(String id : filterCriteria.getContactsids()) {
    			idsBooleanQueryBuilder.should(termQuery("id", id)) ;
    		}
    		boolQueryBuilder.must(idsBooleanQueryBuilder) ;
    	}
    	
    	//搜索条件
    	if(!StringUtils.isBlank(filterCriteria.getName())) {
    		boolQueryBuilder.must(new BoolQueryBuilder().should(QueryBuilders.matchPhraseQuery("name",filterCriteria.getName())).should(QueryBuilders.wildcardQuery("name","*"+filterCriteria.getName()+"*"))) ;
		}
    	if(!StringUtils.isBlank(filterCriteria.getCreater())) {
    		boolQueryBuilder.must(termQuery("creater", filterCriteria.getCreater())) ;
		}
    	if(!StringUtils.isBlank(filterCriteria.getCkind())) {
    		boolQueryBuilder.must(termQuery("ckind", filterCriteria.getCkind())) ;
		}
    	if(!StringUtils.isBlank(filterCriteria.getCkinds())) {
    		boolQueryBuilder.must(termQuery("ckind", filterCriteria.getCkinds())) ;
		}
    	if(!StringUtils.isBlank(filterCriteria.getGender())) {
    		boolQueryBuilder.must(termQuery("gender", filterCriteria.getGender())) ;
		}
    	if(!StringUtils.isBlank(filterCriteria.getPhone())) {
    		boolQueryBuilder.must(termQuery("phone", filterCriteria.getPhone())) ;
		}
    	if(!StringUtils.isBlank(filterCriteria.getMobileno())) {
    		boolQueryBuilder.must(termQuery("mobileno", filterCriteria.getMobileno())) ;
		}
    	if(!StringUtils.isBlank(filterCriteria.getEmail())) {
    		boolQueryBuilder.must(new BoolQueryBuilder().should(QueryBuilders.matchPhraseQuery("email",filterCriteria.getEmail())).should(QueryBuilders.wildcardQuery("email","*"+filterCriteria.getEmail()+"*"))) ;
		}
    	if(!StringUtils.isBlank(filterCriteria.getAddress())) {
    		boolQueryBuilder.must(new BoolQueryBuilder().should(QueryBuilders.matchPhraseQuery("address",filterCriteria.getAddress())).should(QueryBuilders.wildcardQuery("address","*"+filterCriteria.getAddress()+"*"))) ;
		}
    	if(!StringUtils.isBlank(filterCriteria.getProvince())) {
    		boolQueryBuilder.must(termQuery("province", filterCriteria.getProvince())) ;
		}
    	if(!StringUtils.isBlank(filterCriteria.getCity())) {
    		boolQueryBuilder.must(termQuery("city", filterCriteria.getCity())) ;
    	}
    	if(!StringUtils.isBlank(filterCriteria.getCtype())) {
    		boolQueryBuilder.must(termQuery("ctype", filterCriteria.getCtype())) ;
    	}
    	
    	//客户
    	if(!StringUtils.isBlank(filterCriteria.getCusname())) {
    		boolQueryBuilder.must(new BoolQueryBuilder().should(QueryBuilders.matchPhraseQuery("cusname",filterCriteria.getCusname())).should(QueryBuilders.wildcardQuery("cusname","*"+filterCriteria.getCusname()+"*"))) ;
		}
    	if(!StringUtils.isBlank(filterCriteria.getCusphone())) {
    		boolQueryBuilder.must(termQuery("cusphone", filterCriteria.getCusphone())) ;
		}
    	if(!StringUtils.isBlank(filterCriteria.getCusemail())) {
    		boolQueryBuilder.must(new BoolQueryBuilder().should(QueryBuilders.matchPhraseQuery("cusemail",filterCriteria.getCusemail())).should(QueryBuilders.wildcardQuery("cusemail","*"+filterCriteria.getCusemail()+"*"))) ;
		}
    	if(!StringUtils.isBlank(filterCriteria.getCusprovince())) {
    		boolQueryBuilder.must(termQuery("cusprovince", filterCriteria.getCusprovince())) ;
		}
    	if(!StringUtils.isBlank(filterCriteria.getCuscity())) {
    		boolQueryBuilder.must(termQuery("cuscity", filterCriteria.getCuscity())) ;
    	}
    	if(!StringUtils.isBlank(filterCriteria.getCusaddress())) {
    		boolQueryBuilder.must(new BoolQueryBuilder().should(QueryBuilders.matchPhraseQuery("cusaddress",filterCriteria.getCusaddress())).should(QueryBuilders.wildcardQuery("cusaddress","*"+filterCriteria.getCusaddress()+"*"))) ;
		}
    	if(!StringUtils.isBlank(filterCriteria.getEkind())) {
    		boolQueryBuilder.must(termQuery("ekind", filterCriteria.getEkind())) ;
		}
    	if(!StringUtils.isBlank(filterCriteria.getElevel())) {
    		boolQueryBuilder.must(termQuery("ekind", filterCriteria.getElevel())) ;
		}
    	if(!StringUtils.isBlank(filterCriteria.getEsource())) {
    		boolQueryBuilder.must(termQuery("ekind", filterCriteria.getEsource())) ;
		}
    	if(!StringUtils.isBlank(filterCriteria.getMaturity())) {
    		boolQueryBuilder.must(termQuery("maturity", filterCriteria.getMaturity())) ;
		}
    	if(!StringUtils.isBlank(filterCriteria.getIndustry())) {
    		boolQueryBuilder.must(termQuery("industry", filterCriteria.getIndustry())) ;
    	}
    	if(!StringUtils.isBlank(filterCriteria.getCusvalidstatus())) {
    		boolQueryBuilder.must(termQuery("cusvalidstatus", filterCriteria.getCusvalidstatus())) ;
    	}
    	if(!StringUtils.isBlank(filterCriteria.getSalestatus())) {
    		boolQueryBuilder.must(termQuery("salestatus", filterCriteria.getSalestatus())) ;
    	}
    	if(!StringUtils.isBlank(filterCriteria.getStatus())) {
    		boolQueryBuilder.must(termQuery("status", filterCriteria.getStatus())) ;
    	}
    	if(!StringUtils.isBlank(filterCriteria.getReservation())) {
    		boolQueryBuilder.must(termQuery("reservation", filterCriteria.getReservation().equals("1")?true:false)) ;
    	}
    	if(!StringUtils.isBlank(filterCriteria.getReservtype())) {
    		boolQueryBuilder.must(termQuery("reservtype", filterCriteria.getReservtype())) ;
    	}
    	if(!StringUtils.isBlank(filterCriteria.getSalesmemo())) {
    		boolQueryBuilder.must(new BoolQueryBuilder().should(QueryBuilders.matchPhraseQuery("salesmemo",filterCriteria.getSalesmemo())).should(QueryBuilders.wildcardQuery("salesmemo","*"+filterCriteria.getSalesmemo()+"*"))) ;
    	}
    	if(!StringUtils.isBlank(filterCriteria.getUpdateuser())) {
    		boolQueryBuilder.must(termQuery("updateuser", filterCriteria.getUpdateuser())) ;
    	}
    	if(!StringUtils.isBlank(filterCriteria.getBusiness())) {
    		boolQueryBuilder.must(termQuery("business", filterCriteria.getBusiness())) ;
    	}
    	if(!StringUtils.isBlank(filterCriteria.getBusmemo())) {
    		boolQueryBuilder.must(new BoolQueryBuilder().should(QueryBuilders.matchPhraseQuery("busmemo",filterCriteria.getBusmemo())).should(QueryBuilders.wildcardQuery("busmemo","*"+filterCriteria.getBusmemo()+"*"))) ;
    	}
    	
    	RangeQueryBuilder rangeQuery = null ;
		//创建时间区间查询
		if(!StringUtils.isBlank(filterCriteria.getTouchtimebegin()) || !StringUtils.isBlank(filterCriteria.getTouchtimeend())){
			if(!StringUtils.isBlank(filterCriteria.getTouchtimebegin())) {
				try {
					rangeQuery = QueryBuilders.rangeQuery("touchtime").from(UKTools.dateFormate.parse(filterCriteria.getTouchtimebegin()).getTime()) ;
				} catch (ParseException e) {
					e.printStackTrace();
				}
			}
			if(!StringUtils.isBlank(filterCriteria.getTouchtimeend()) ) {
				try {
					if(rangeQuery == null) {
						rangeQuery = QueryBuilders.rangeQuery("touchtime").to(UKTools.dateFormate.parse(filterCriteria.getTouchtimeend()).getTime()) ;
					}else {
						rangeQuery.to(UKTools.dateFormate.parse(filterCriteria.getTouchtimeend()).getTime()) ;
					}
				} catch (ParseException e) {
					e.printStackTrace();
				}
			}
		}
		if(rangeQuery!=null) {
			boolQueryBuilder.must(rangeQuery) ;
		}
		rangeQuery = null ;
		if(!StringUtils.isBlank(filterCriteria.getIncallbegin()) || !StringUtils.isBlank(filterCriteria.getIncallend())){
			if(!StringUtils.isBlank(filterCriteria.getIncallbegin())) {
				rangeQuery = QueryBuilders.rangeQuery("incall").from(Long.parseLong(filterCriteria.getIncallbegin())) ;
			}
			if(!StringUtils.isBlank(filterCriteria.getIncallend()) ) {
				if(rangeQuery == null) {
					rangeQuery = QueryBuilders.rangeQuery("incall").to(Long.parseLong(filterCriteria.getIncallend())) ;
				}else {
					rangeQuery.to(Long.parseLong(filterCriteria.getIncallend())) ;
				}
			}
		}
		if(rangeQuery!=null) {
			boolQueryBuilder.must(rangeQuery) ;
		}
		rangeQuery = null ;
		if(!StringUtils.isBlank(filterCriteria.getOptimebegin()) || !StringUtils.isBlank(filterCriteria.getOptimeend())){
			if(!StringUtils.isBlank(filterCriteria.getOptimebegin())) {
				try {
					rangeQuery = QueryBuilders.rangeQuery("optime").from(UKTools.dateFormate.parse(filterCriteria.getOptimebegin()).getTime()) ;
				} catch (ParseException e) {
					e.printStackTrace();
				}
			}
			if(!StringUtils.isBlank(filterCriteria.getOptimeend()) ) {
				try {
					if(rangeQuery == null) {
						rangeQuery = QueryBuilders.rangeQuery("optime").to(UKTools.dateFormate.parse(filterCriteria.getOptimeend()).getTime()) ;
					}else {
						rangeQuery.to(UKTools.dateFormate.parse(filterCriteria.getOptimeend()).getTime()) ;
					}
				} catch (ParseException e) {
					e.printStackTrace();
				}
			}
		}
		if(rangeQuery!=null) {
			boolQueryBuilder.must(rangeQuery) ;
		}
		rangeQuery = null ;
		if(!StringUtils.isBlank(filterCriteria.getUpdatetimebegin()) || !StringUtils.isBlank(filterCriteria.getUpdatetimeend())){
			if(!StringUtils.isBlank(filterCriteria.getUpdatetimebegin())) {
				try {
					rangeQuery = QueryBuilders.rangeQuery("updatetime").from(UKTools.dateFormate.parse(filterCriteria.getUpdatetimebegin()).getTime()) ;
				} catch (ParseException e) {
					e.printStackTrace();
				}
			}
			if(!StringUtils.isBlank(filterCriteria.getUpdatetimeend()) ) {
				try {
					if(rangeQuery == null) {
						rangeQuery = QueryBuilders.rangeQuery("updatetime").to(UKTools.dateFormate.parse(filterCriteria.getUpdatetimeend()).getTime()) ;
					}else {
						rangeQuery.to(UKTools.dateFormate.parse(filterCriteria.getUpdatetimeend()).getTime()) ;
					}
				} catch (ParseException e) {
					e.printStackTrace();
				}
			}
		}
		if(rangeQuery!=null) {
			boolQueryBuilder.must(rangeQuery) ;
		}
    	return boolQueryBuilder ;
	}
   
}
