package com.ukefu.webim.service.robot;

import com.ukefu.webim.web.model.AiUser;

public class RobotReturnParam implements java.io.Serializable{

	private static final long serialVersionUID = 1694023673434563114L;


	/*{
		"queueid":"8888",		 (transfer的目标号码,默认为0无效，<0表示挂机,>0 表示transfer目标号码),
		"playvoice":"test.wav",  (播音的语音,不会打断,多个文件使用分号;分开)
		"asrvoice":"ok.wav",  	(asr语音,多个文件使用分号;分开)
		"asrtimeout":"10",   识别超时时间,单位秒,没有默认5s,>=1 有效
		"param1":"1",   扩展参数1,通常使用来保存会话上下文参数,ivr会把这个返回给web
		"param2":"1",    扩展参数2,="-9"表示识别出来的文本没匹配到关键字,暂停的语音继续播放
		"sos_eng":"",   用户开始说话的声音阀值,越小越敏感,一般800,  >0有效
		"eos_eng":"",   用户说完之后没说话的静音阈值,一般400, >0有效
		"eos_time":""   用户说话停顿超时，说话结束,单位ms,越小越容易结束一般500, >0有效
	}*/


	private String queueid = "0";
	private String playvoice = "";
	private String asrvoice= "";
	private String asrtimeout = "10";

	private String param1 = "1";

	private String param2 = "";
	private String sos_eng = "800";
	private String eos_eng = "400";
	private String eos_time = "500";

	public RobotReturnParam() {

	}

	public RobotReturnParam(AiUser aiUser) {
		if(aiUser != null){
			this.sos_eng = String.valueOf(aiUser.getSos_eng());
			this.eos_eng = String.valueOf(aiUser.getEos_eng());
			this.eos_time = String.valueOf(aiUser.getEos_time());
		}

	}

	public String getQueueid() {
		return queueid;
	}

	public void setQueueid(String queueid) {
		this.queueid = queueid;
	}

	public String getPlayvoice() {
		return playvoice;
	}

	public void setPlayvoice(String playvoice) {
		this.playvoice = playvoice;
	}

	public String getAsrvoice() {
		return asrvoice;
	}

	public void setAsrvoice(String asrvoice) {
		this.asrvoice = asrvoice;
	}

	public String getAsrtimeout() {
		return asrtimeout;
	}

	public void setAsrtimeout(String asrtimeout) {
		this.asrtimeout = asrtimeout;
	}

	public String getParam1() {
		return param1;
	}

	public void setParam1(String param1) {
		this.param1 = param1;
	}

	public String getParam2() {
		return param2;
	}

	public void setParam2(String param2) {
		this.param2 = param2;
	}

	public String getSos_eng() {
		return sos_eng;
	}

	public void setSos_eng(String sos_eng) {
		this.sos_eng = sos_eng;
	}

	public String getEos_eng() {
		return eos_eng;
	}

	public void setEos_eng(String eos_eng) {
		this.eos_eng = eos_eng;
	}

	public String getEos_time() {
		return eos_time;
	}

	public void setEos_time(String eos_time) {
		this.eos_time = eos_time;
	}


	@Override
	public String toString() {
		return "RobotReturnParam{" +
				"queueid='" + queueid + '\'' +
				", playvoice='" + playvoice + '\'' +
				", asrvoice='" + asrvoice + '\'' +
				", asrtimeout='" + asrtimeout + '\'' +
				", param1='" + param1 + '\'' +
				", param2='" + param2 + '\'' +
				", sos_eng='" + sos_eng + '\'' +
				", eos_eng='" + eos_eng + '\'' +
				", eos_time='" + eos_time + '\'' +
				'}';
	}
}
