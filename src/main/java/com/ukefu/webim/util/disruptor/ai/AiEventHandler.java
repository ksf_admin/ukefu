package com.ukefu.webim.util.disruptor.ai;

import com.lmax.disruptor.EventHandler;
import com.ukefu.core.UKDataContext;
import com.ukefu.util.event.AiEvent;

public class AiEventHandler implements EventHandler<AiEvent>{
	
	/**
	 * AI 处理回复
	 */
	@Override
	public void onEvent(AiEvent event, long arg1, boolean arg2)
			throws Exception {
		if(UKDataContext.model.get("xiaoe") != null){
			AiMessageProcesserInterface aiMessageProcesser = UKDataContext.getContext().getBean(AiMessageProcesserInterface.class) ;
			aiMessageProcesser.onEvent(event, arg1, arg2);
		}
	}

}
