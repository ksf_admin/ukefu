package com.ukefu.webim.web.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

/**
 * 批次表，导入批次
 * @author iceworld
 *
 */
@Entity
@Table(name = "uk_jobdetail")
@org.hibernate.annotations.Proxy(lazy = false)
public class UpdateJobDetail implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2258870729818431384L;
	
	private String id ;
	
	private String orgi ;		//租户ID
	private String memo;		
	
	
	@Id
	@Column(length = 32)
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "uuid")	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getOrgi() {
		return orgi;
	}

	public void setOrgi(String orgi) {
		this.orgi = orgi;
	}

	public String getMemo() {
		return memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}
}
