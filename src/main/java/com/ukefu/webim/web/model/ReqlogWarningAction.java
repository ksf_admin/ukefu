package com.ukefu.webim.web.model;

public class ReqlogWarningAction implements java.io.Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8083852888814162079L;
	private String warnid ;//预警内容id 来自字典
	private String actionid;//动作id 来自字典
	
	
	public String getWarnid() {
		return warnid;
	}
	public void setWarnid(String warnid) {
		this.warnid = warnid;
	}
	public String getActionid() {
		return actionid;
	}
	public void setActionid(String actionid) {
		this.actionid = actionid;
	}
	
	
}
