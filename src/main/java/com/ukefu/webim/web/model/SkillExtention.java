package com.ukefu.webim.web.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "uk_callcenter_skillext")
@org.hibernate.annotations.Proxy(lazy = false)
public class SkillExtention implements java.io.Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 3932323765657445180L;
	private String id;
	private String name;
	private String orgi;
	private String creater ;
	private String type;
	private Date createtime = new Date();
	private Date updatetime = new Date();
	private String skillid ;
	private String extention ;
	private String hostid ;
	
	private String status;//状态
	private int maxnoanswer;//最大无应答
	private int wrapuptime;//话后处理时间
	private int redeltime;//客户端拒绝后延迟时间
	private int budeltime;//忙延迟时间
	private int level;//级别
	private int position;//地位
	
	@Id
	@Column(length = 32)
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "uuid")	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getOrgi() {
		return orgi;
	}
	public void setOrgi(String orgi) {
		this.orgi = orgi;
	}
	public String getCreater() {
		return creater;
	}
	public void setCreater(String creater) {
		this.creater = creater;
	}
	public Date getCreatetime() {
		return createtime;
	}
	public void setCreatetime(Date createtime) {
		this.createtime = createtime;
	}
	public Date getUpdatetime() {
		return updatetime;
	}
	public void setUpdatetime(Date updatetime) {
		this.updatetime = updatetime;
	}
	public String getSkillid() {
		return skillid;
	}
	public void setSkillid(String skillid) {
		this.skillid = skillid;
	}
	public String getExtention() {
		return extention;
	}
	public void setExtention(String extention) {
		this.extention = extention;
	}
	public String getHostid() {
		return hostid;
	}
	public void setHostid(String hostid) {
		this.hostid = hostid;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public int getMaxnoanswer() {
		return maxnoanswer;
	}
	public void setMaxnoanswer(int maxnoanswer) {
		this.maxnoanswer = maxnoanswer;
	}
	public int getWrapuptime() {
		return wrapuptime;
	}
	public void setWrapuptime(int wrapuptime) {
		this.wrapuptime = wrapuptime;
	}
	public int getRedeltime() {
		return redeltime;
	}
	public void setRedeltime(int redeltime) {
		this.redeltime = redeltime;
	}
	public int getBudeltime() {
		return budeltime;
	}
	public void setBudeltime(int budeltime) {
		this.budeltime = budeltime;
	}
	public int getLevel() {
		return level;
	}
	public void setLevel(int level) {
		this.level = level;
	}
	public int getPosition() {
		return position;
	}
	public void setPosition(int position) {
		this.position = position;
	}
	
}
