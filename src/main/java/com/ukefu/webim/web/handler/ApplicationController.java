package com.ukefu.webim.web.handler;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.ukefu.core.UKDataContext;
import com.ukefu.util.Menu;
import com.ukefu.webim.service.acd.ServiceQuene;
import com.ukefu.webim.service.cache.CacheHelper;
import com.ukefu.webim.service.repository.CrmSchemeRepository;
import com.ukefu.webim.service.repository.EkmKnowbaseConfigRepository;
import com.ukefu.webim.service.repository.EkmKnowbaseRepository;
import com.ukefu.webim.service.repository.ExtentionRepository;
import com.ukefu.webim.service.repository.UserRepository;
import com.ukefu.webim.web.model.CrmScheme;
import com.ukefu.webim.web.model.EkmKnowbase;
import com.ukefu.webim.web.model.EkmKnowbaseConfig;
import com.ukefu.webim.web.model.Role;
import com.ukefu.webim.web.model.SysDic;
import com.ukefu.webim.web.model.UKeFuDic;
import com.ukefu.webim.web.model.User;

@Controller
public class ApplicationController extends Handler{
	
	@Autowired
	private ExtentionRepository extentionRes;
	
	@Autowired
	private UserRepository userRes;
	
	
	@Autowired
	private EkmKnowbaseRepository ekmKnowbaseRes;
	
	@Autowired
	private EkmKnowbaseConfigRepository ekmKnowbaseConfigRes;
	
	@Autowired
	private CrmSchemeRepository crmSchemeRes ;
	
	@RequestMapping("/")
	@Menu(type = "apps" , subtype = "root" , access=true)
    public ModelAndView root(HttpServletRequest request) {
		ModelAndView view = console(request);
		if(!StringUtils.isBlank(request.getServerName())) {
			EkmKnowbaseConfig kbconfig = ekmKnowbaseConfigRes.findByBasehost(request.getServerName()) ;
			if(kbconfig != null) {
				EkmKnowbase base = ekmKnowbaseRes.findByIdAndOrgi(kbconfig.getKnowbaseid(),super.getOrgi(request)) ;
				view = request(super.createRequestPageTempletResponse("redirect:/helpdesk/"+base.getKbviewid()+".html"));
			}
		}
		return view ;
	}
	
	
	@RequestMapping("/console")
    public ModelAndView console(HttpServletRequest request) {
		ModelAndView view = request(super.createRequestPageTempletResponse("/apps/index"));
		User user = super.getUser(request) ;
        view.addObject("agentStatusReport",ServiceQuene.getAgentReport(user.getOrgi())) ;
        //NOTE 注释多租户
        /*view.addObject("tenant",super.getTenant(request));
        view.addObject("istenantshare",super.isEnabletneant());*/

        if(user.getRoleList()!=null && user.getRoleList().size() > 0) {
        	for(Role role : user.getRoleList()) {
        		if(!StringUtils.isBlank(role.getPortal())) {
        			SysDic dic = UKeFuDic.getInstance().getDicItem(role.getPortal()) ;
        			if(dic!=null && !StringUtils.isBlank(dic.getDescription()) && !dic.getDescription().equals("/") && !dic.getDescription().equals("/console")) {
	        			if(UKDataContext.PortalType.CONTACTS.toString().equals(dic.getCode()) && UKDataContext.model.get("callcenter")!=null) {
	        				view.addObject("portal",dic.getDescription());
		        		}
        			}
        			break ;
        		}
        	}
        }
        
		view.addObject("agentStatus",CacheHelper.getAgentStatusCacheBean().getCacheObject(user.getId(), user.getOrgi())) ;
		view.addObject("sessionConfig",ServiceQuene.initSessionConfig(super.getOrgi(request))) ;
		
		List<CrmScheme> schemeList = crmSchemeRes.findByOrgi(user.getOrgi()) ;
		if (schemeList != null ) {
			view.addObject("schemeList", schemeList) ;
		}
    	view.addObject("crm_scheme", schemeList != null && schemeList.size() > 0 ? schemeList.get(0).getCode() : "crm") ;
        return view;
    }
	
	@RequestMapping("/menu")
    public ModelAndView menu(HttpServletRequest request) {
        return request(super.createRequestPageTempletResponse("/apps/include/menu"));
    }
	
	@RequestMapping("/ext/autologin")
    public ModelAndView autologin(ModelMap map , HttpServletRequest request) {
		User user = userRes.findByIdAndOrgi(super.getUser(request).getId(),super.getOrgi(request)) ;
		if(UKDataContext.model.get("callcenter")!=null && user.isBindext() && !StringUtils.isBlank(user.getExtid())) {
			map.addAttribute("ext",extentionRes.findByIdAndOrgi(user.getExtid() , super.getOrgi(request))) ;
		}
        return request(super.createRequestPageTempletResponse("/apps/include/autologin"));
    }
}