package com.ukefu.webim.web.handler.admin.callcenter;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.ukefu.util.Menu;
import com.ukefu.webim.service.repository.PbxHostOrgiRelaRepository;
import com.ukefu.webim.service.repository.PbxHostRepository;
import com.ukefu.webim.service.repository.TenantRepository;
import com.ukefu.webim.web.handler.Handler;
import com.ukefu.webim.web.model.PbxHost;
import com.ukefu.webim.web.model.PbxHostOrgiRela;
import com.ukefu.webim.web.model.Tenant;

@Controller
@RequestMapping("/admin/callcenter/pbxhostorgirela")
public class CallCenterPbxHostOrgiRela extends Handler{

	@Autowired
	private PbxHostOrgiRelaRepository pbxHostOrgiRelaRes ;
	
	@Autowired
	private TenantRepository tenantRes ;
	
	@Autowired
	private PbxHostRepository pbxHostRes ;
	
	@RequestMapping(value = "/index")
    @Menu(type = "callcenter" , subtype = "pbxhostorgirela" , access = false )
    public ModelAndView pbxhostorgirela(ModelMap map , HttpServletRequest request , @Valid String hostid) {
		List<PbxHost> pbxHostList = pbxHostRes.findByOrgi(super.getOrgi(request)) ;
		map.addAttribute("pbxHostList" , pbxHostList);
		PbxHost pbxHost = null ;
		if(pbxHostList.size() > 0){
			map.addAttribute("pbxHost" , pbxHost = getPbxHost(pbxHostList, hostid));
			List<PbxHostOrgiRela> relaList = pbxHostOrgiRelaRes.findByPbxhostid(pbxHost.getId());
			List<String> ids = new ArrayList<String>();
			if(relaList != null && !relaList.isEmpty()) {
				for(PbxHostOrgiRela pbxHostOrgiRela : relaList) {
					ids.add(pbxHostOrgiRela.getOrgi());
				}
				if (ids != null && ids.size() > 0) {
					Page<Tenant> tenantList = tenantRes.findByTenantcodeIn(ids,new PageRequest(super.getP(request), super.getPs(request), Sort.Direction.DESC,new String[] {"createtime"}));
					map.addAttribute("userList", tenantList);
				}
			}
			
			
		}
		return request(super.createRequestPageTempletResponse("/admin/callcenter/pbxhostorgirela/index"));
    }
	
	private PbxHost getPbxHost(List<PbxHost> pbxHostList ,String hostid){
		PbxHost pbxHost = pbxHostList.get(0) ;
		if(!StringUtils.isBlank(hostid)){
			for(PbxHost pbx : pbxHostList){
				if(pbx.getId().equals(hostid)){
					pbxHost = pbx; break ;
				}
			}
		}
		return pbxHost ;
	}
	
	@RequestMapping(value = "/add")
    @Menu(type = "callcenter" , subtype = "pbxhostorgirela" , access = false )
    public ModelAndView add(ModelMap map , HttpServletRequest request , @Valid String hostid) {
		List<Tenant> tenantList = tenantRes.findAll();
		map.addAttribute("tenantList", tenantList);
		List<PbxHostOrgiRela> relaList = pbxHostOrgiRelaRes.findByPbxhostid(hostid);
		StringBuffer str = new StringBuffer();
		if(relaList != null && !relaList.isEmpty()) {
			for(PbxHostOrgiRela pbxHostOrgiRela : relaList) {
				str.append(pbxHostOrgiRela.getOrgi());
				str.append(",");
			}
		}
		map.addAttribute("pbxHostOrgiRelastr", str.toString());
		map.addAttribute("hostid", hostid);
    	return request(super.createRequestPageTempletResponse("/admin/callcenter/pbxhostorgirela/add"));
    }
	
	@RequestMapping(value = "/save")
    @Menu(type = "callcenter" , subtype = "pbxhostorgirela" , access = false )
    public ModelAndView save(ModelMap map , HttpServletRequest request , @Valid String hostid,@Valid String[] tenantcodes) {
		if (tenantcodes != null && tenantcodes.length > 0 && !StringUtils.isBlank(hostid)) {
			List<PbxHostOrgiRela> PbxHostOrgiRelaList = new ArrayList<PbxHostOrgiRela>();
			PbxHostOrgiRela pbxHostOrgiRela = null;
			for(String tenantcode : tenantcodes) {
				pbxHostOrgiRela = new PbxHostOrgiRela();
				pbxHostOrgiRela.setCreater(super.getUser(request).getId());
				pbxHostOrgiRela.setOrgi(tenantcode);
				pbxHostOrgiRela.setPbxhostid(hostid);
				PbxHostOrgiRelaList.add(pbxHostOrgiRela) ;
			}
			if (PbxHostOrgiRelaList != null && PbxHostOrgiRelaList.size() > 0) {
				pbxHostOrgiRelaRes.save(PbxHostOrgiRelaList);
			}
		}
		
		return request(super.createRequestPageTempletResponse("redirect:/admin/callcenter/pbxhostorgirela/index.html?hostid="+hostid));
    }
	
	@RequestMapping(value = "/delete")
    @Menu(type = "callcenter" , subtype = "extention" , access = false )
    public ModelAndView delete(ModelMap map , HttpServletRequest request , @Valid String hostid, @Valid String orgi) {
		if (!StringUtils.isBlank(hostid) && !StringUtils.isBlank(orgi)) {
			List<PbxHostOrgiRela> rela = pbxHostOrgiRelaRes.findByPbxhostidAndOrgi(hostid, orgi);
			if (rela != null && rela.size() > 0) {
				pbxHostOrgiRelaRes.delete(rela);
			}
		}
		return request(super.createRequestPageTempletResponse("redirect:/admin/callcenter/pbxhostorgirela/index.html?hostid="+hostid));
    }
	
}
