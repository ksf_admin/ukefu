package com.ukefu.util;

import org.apache.commons.lang3.StringUtils;

public class IP implements java.io.Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -421278423658892060L;
	private String country ;
	private String province ;
	private String city ;
	private String isp ;
	private String region ;
	public String getCountry() {
		return country != null ? country : "0";
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getProvince() {
		return province != null ? province : "0";
	}
	public void setProvince(String province) {
		this.province = province;
	}
	public String getCity() {
		return city != null ? city : "0";
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getIsp() {
		return isp;
	}
	public void setIsp(String isp) {
		this.isp = isp;
	}
	public String getRegion() {
		return region;
	}
	public void setRegion(String region) {
		this.region = region;
	}
	
	public String toString(){
		StringBuffer strb = new StringBuffer();
		if(!"0".equals(this.country)) {
			strb.append(this.country) ;
		}
		if(!"0".equals(this.province)) {
			strb.append(this.province) ;
		}
		if(!"0".equals(this.city)) {
			strb.append(this.city) ;
		}
		if(strb.length() == 0 && !StringUtils.isBlank(this.getRegion())) {
			strb.append(this.getRegion()) ;
		}
		return strb.toString();
	}
}
